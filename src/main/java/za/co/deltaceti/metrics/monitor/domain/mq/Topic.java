package za.co.deltaceti.metrics.monitor.domain.mq;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import za.co.deltaceti.metrics.monitor.domain.BiMetricThreshold;
import za.co.deltaceti.metrics.monitor.domain.Threshold;

@Getter
@NoArgsConstructor
public class Topic extends Destination implements Comparable<Topic> {
    public Topic(final String name, final Map<String, Object> properties, final List<Threshold> thresholds, final List<BiMetricThreshold> multiThreshold) {
        super(name, properties, thresholds, multiThreshold);
    }

    @Override
    public int compareTo(final Topic o) {
        if (null != o.getBreaches()) {
            return 1;
        }
        return 0;
    }
}

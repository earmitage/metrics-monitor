package za.co.deltaceti.metrics.monitor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import za.co.deltaceti.metrics.monitor.config.ConnectionData;
import za.co.deltaceti.metrics.monitor.domain.mq.Queue;
import za.co.deltaceti.metrics.monitor.persistence.Breach;
import za.co.deltaceti.metrics.monitor.persistence.BreachRepository;
import za.co.deltaceti.metrics.monitor.persistence.QueueManager;
import za.co.deltaceti.metrics.monitor.persistence.QueueManagerRepository;
import za.co.deltaceti.metrics.monitor.persistence.Threshold;
import za.co.deltaceti.metrics.monitor.persistence.ThresholdRepository;

@Slf4j
@Service
@Getter
public class ConnectionRepository {

    @Autowired
    private QueueManagerRepository queueManagerRepository;

    @Autowired
    private BreachRepository breachRepository;

    @Autowired
    private ThresholdRepository thresholdRepository;

    @Cacheable("connections")
    public ConnectionData findByHostAndQueueManager(final String host, final String queueManager) {
        return queueManagerRepository.findByHostnameAndQueueManagerName(host, queueManager).connectionData();
    }

    public void addConnectionData(final ConnectionData connectionData) {
        final QueueManager queueManager = queueManagerRepository.save(new za.co.deltaceti.metrics.monitor.persistence.QueueManager(connectionData));
        connectionData.getProperties().getThresholds().forEach(threshold -> thresholdRepository.save(new Threshold(queueManager, threshold)));

    }

    public List<za.co.deltaceti.metrics.monitor.persistence.QueueManager> findAll() {
        return queueManagerRepository.findAll();
    }

    public void recordBreach(final ConnectionData connectionData, final Queue queue) {
        if (queue.inBreach()) {
            queue.getBreaches().forEach(breach -> {
                final Threshold threshold = thresholdRepository.findByUuid(breach.getThreshold().getUuid());
                breachRepository.save(new Breach(queue.getName(), threshold, breach.getActualValue()));
                log.info("Recorded breach {}", queue.getBreaches());
            });
        }
    }

}

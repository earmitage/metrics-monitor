package za.co.deltaceti.metrics.monitor.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;
import za.co.deltaceti.metrics.monitor.domain.mq.MqConnectionProperties;

@Getter
@Setter
@ConfigurationProperties("browser")
public class MessageBrowserProperties {

    private String metricsCollectionFrequency;
    private List<MqConnectionProperties> monitored;
}

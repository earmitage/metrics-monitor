package za.co.deltaceti.metrics.monitor.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BiMetricThreshold {

    private String name;
    private Threshold first;
    private Operand operand;
    private Threshold second;
}

package za.co.deltaceti.metrics.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetricsMonitorApplication {

    public static void main(final String[] args) {
        SpringApplication.run(MetricsMonitorApplication.class, args);
    }

}

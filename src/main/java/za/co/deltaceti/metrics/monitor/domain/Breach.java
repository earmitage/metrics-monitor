package za.co.deltaceti.metrics.monitor.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
public class Breach {

    private final Threshold threshold;
    private final long actualValue;

}

package za.co.deltaceti.metrics.monitor.domain;

import java.util.function.BiFunction;

public enum Condition {

    LESS_THAN((a, b) -> ((Comparable) a).compareTo(b) < 0),
    GREATER_THAN((a, b) -> ((Comparable) a).compareTo(b) > 0),
    EQUALS((a, b) -> a.equals(b));

    final BiFunction<Comparable<?>, Comparable<?>, Boolean> function;

    <T> Condition(final BiFunction<Comparable<?>, Comparable<?>, Boolean> function) {
        this.function = function;
    }

    public boolean apply(final Comparable<?> a, final Comparable<?> b) {
        return function.apply(a, b);
    }
}

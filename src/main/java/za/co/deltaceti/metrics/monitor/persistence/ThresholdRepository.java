package za.co.deltaceti.metrics.monitor.persistence;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "thresholds", path = "thresholds")
public interface ThresholdRepository extends PagingAndSortingRepository<Threshold, Long> {

    Threshold findByUuid(@Param("uuid") final String uuid);

}

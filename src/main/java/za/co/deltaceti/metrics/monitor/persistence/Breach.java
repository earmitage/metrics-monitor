
package za.co.deltaceti.metrics.monitor.persistence;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@Entity
public class Breach {

    public static final String SEQUENCE_NAME = "mm_breach_seq";
    @Id
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
    @JsonIgnore
    private Long id;

    private final String uuid = UUID.randomUUID().toString();

    private String destinationName;
    @ManyToOne
    private Threshold threshold;
    private long actualValue;

    public Breach(final String destinationName, final Threshold threshold, final long actualValue) {
        this.threshold = threshold;
        this.actualValue = actualValue;
        this.destinationName = destinationName;
    }

}

package za.co.deltaceti.metrics.monitor.domain.mq;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;
import za.co.deltaceti.metrics.monitor.domain.BiMetricThreshold;
import za.co.deltaceti.metrics.monitor.domain.Threshold;

@Getter
@Setter
public class MqConnectionProperties {

    private String name;
    private String monitorFrequency;
    private String hostname;
    private int port;
    private String queueManager;
    private String channel;
    private String userName;
    private String password;
    private int transportType;
    private List<Threshold> thresholds = new ArrayList<>();
    private List<BiMetricThreshold> multiThreshold = new ArrayList<>();

    public List<Threshold> thresholdsForDestination(final String destinationName) {
        return thresholds.stream().filter(threshold -> threshold.getDestinationName().equals(destinationName))
                .collect(Collectors.toList());
    }

    public List<BiMetricThreshold> multiThresholdForDestination(final String destinationName) {
        return multiThreshold.stream().filter(threshold -> (threshold.getFirst().getDestinationName().equals(destinationName) || threshold.getSecond().getDestinationName().equals(destinationName)))
                .collect(Collectors.toList());
    }

}

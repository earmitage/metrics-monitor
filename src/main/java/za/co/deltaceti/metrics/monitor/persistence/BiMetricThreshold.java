
package za.co.deltaceti.metrics.monitor.persistence;

import static javax.persistence.GenerationType.SEQUENCE;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import za.co.deltaceti.metrics.monitor.domain.Operand;

@Entity
@Getter
@Setter
public class BiMetricThreshold {

    public static final String SEQUENCE_NAME = "mm_bm_thresh_seq";

    @Id
    @GeneratedValue(generator = SEQUENCE_NAME, strategy = SEQUENCE)
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, initialValue = 1, allocationSize = 1)
    @JsonIgnore
    private Long id;

    private final String uuid = UUID.randomUUID().toString();

    private String name;

    @ManyToOne
    private QueueManager queueManager;

    @OneToOne
    private Threshold first;

    @Enumerated(EnumType.STRING)
    private Operand operand;

    @OneToOne
    private Threshold second;
}

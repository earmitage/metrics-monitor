package za.co.deltaceti.metrics.monitor.domain.mq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import za.co.deltaceti.metrics.monitor.domain.BiMetricThreshold;
import za.co.deltaceti.metrics.monitor.domain.Breach;
import za.co.deltaceti.metrics.monitor.domain.Threshold;

@Getter
@NoArgsConstructor
public class Destination {

    private String name;
    private Map<String, Object> properties;
    private List<Threshold> thresholds;
    private List<BiMetricThreshold> multiThreshold;
    private final List<Breach> breaches = new ArrayList<>();

    public Destination(final String name, final Map<String, Object> properties, final List<Threshold> thresholds, final List<BiMetricThreshold> multiThreshold) {
        this.name = name;
        this.properties = properties;
        this.thresholds = thresholds;
        this.multiThreshold = multiThreshold;
        checkThresholds();
    }

    private void checkThresholds() {
        thresholds.forEach(threshold -> {
            final Object value = properties.get(threshold.getMqPropertyName());
            if (value != null && !threshold.isTime()) {
                // number
                final long numericValue = Long.parseLong(value.toString());
                final boolean breach = threshold.getCondition().apply(numericValue, Long.valueOf(threshold.getValue()));
                if (breach) {
                    breaches.add(new Breach(threshold, numericValue));
                }
            }

        });
    }

}

package za.co.deltaceti.metrics.monitor.persistence;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "breaches", path = "breaches")
public interface BreachRepository extends PagingAndSortingRepository<Breach, Long> {

}

package za.co.deltaceti.metrics.monitor.domain;

public enum Operand {
    AND, OR
}
